var properties = require('../properties/properties.js')
var url = require('url');
var fs = require('fs');
var logger=require('../logger/logger.js');

function renderHTML(path,response){
	fs.readFile(path,null,function(error,data){
		if(error){
			response.writeHead(404);
			response.write('File not found!');
		}else{
			//response.write(data);
			response.write(data.toString().replace(/SERVERIP/g,"\""+properties.SERVERIP+"\"").replace(/SERVERPORT/g,"\""+properties.SERVERPORT+"\""));
		}
		response.end();
	});
}

function getRequestBody(request){
	let body = [];
	request.on('data', (chunk) => {
	  	body.push(chunk);
	}).on('end', () => {
	    body = Buffer.concat(body).toString();
	});

	try{
		return JSON.parse(body);
	}catch(e){
		logger.info(body+"::body data");
	}finally{
		return null;
	}
}

module.exports = {
	handleRequest: function(request, response){
		response.writeHead(200, {'Content-Type':'text/html'});
		var path = url.parse(request.url).pathname;
		logger.info(request.method);
		switch(request.method){
			case 'GET':
				switch(path){
				  case '/':
					renderHTML('../front/login.html', response);
					logger.info(request.connection.remoteAddress +': 로그인 페이지 접속!');
					break;
				  case '/main':
				    renderHTML('../front/main.html', response);
					logger.info(request.connection.remoteAddress +': 메인 페이지 접속!');
				    break;
				  default:
	   			    logger.info(request.connection.remoteAddress +': 로그인 페이지 접속!');
				    renderHTML('../front/login.html', response);
				    break;
				}
				break;
			case 'POST':
				switch(path){
					case '/login':
						logger.info(request.connection.remoteAddress+":" +getRequestBody(request).user_id+"로 로그인 시도");
						break;
					default:
						logger.info(request.connection.remoteAddress+":" +request.body);
						break;
				}
			case 'DELETE':
			case 'PUT':
			default:
				break;
		}
	}
};
